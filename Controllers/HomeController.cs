﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using GridMvc.Html;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var students = GetStudents();
            return View(students);
        }

            private List<Student> GetStudents()
            {
                var list = new List<Student>();


                var student1 = new Student();
                student1.Rollno = 1;
                student1.Sname = "Apoorva";
                student1.Std = "4";

                var student2 = new Student();
                student2.Rollno = 2;
                student2.Sname = "Krishna";
                student2.Std = "5";

                var student3 = new Student();
                student3.Rollno = 3;
                student3.Sname = "Suriya";
                student3.Std = "2";

                var student4 = new Student();
                student4.Rollno = 4;
                student4.Sname = "Naill";
                student4.Std = "7";

                list.Add(student1);
                list.Add(student2);
                list.Add(student3);
                list.Add(student4);

                return list;
            }
        }


    }
