﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Models
{
    public class Student
    {
        public int Rollno { get; set; }
        public string Sname { get; set; }
        public string Std  { get; set; }
    }
}